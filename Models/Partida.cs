﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonProjecte.Models
{
    public class Partida
    {
        public int PartidaID { get; set; }
        public int JugadorID { get; set; }
        public virtual Jugador Jugador { get; set; }
        public int? CartaPokemonId { get; set; }
        public virtual CartaPokemon CartaPokemon { get; set; }
        public int? CartaEnergiaEntrenadorId { get; set; }
        public virtual CartaEnergiaEntrenador CartaEnergiaEntrenador { get; set; }
        public int? Vida { get; set; }
        public Estat Estat { get; set; }

        public Partida()
        {
        }

        public Partida(Jugador player, CartaPokemon pokemon, int vida)
        {
            Jugador = player;
            CartaPokemon = pokemon;
            Vida = vida;
            Estat = Estat.Pila;
        }

        public Partida(Jugador player, CartaEnergiaEntrenador cartaNoPokemon)
        {
            Jugador = player;
            CartaEnergiaEntrenador = cartaNoPokemon;
            Estat = Estat.Pila;
        }
    }

    public enum Estat
    {
        Pila,
        Ma,
        Activa,
        Descartada
    }
}
