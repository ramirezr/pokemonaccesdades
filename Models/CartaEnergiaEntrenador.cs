﻿namespace PokemonProjecte.Models
{
    public class CartaEnergiaEntrenador
    {
        public int CartaEnergiaEntrenadorID { get; set; }
        public string Nom { get; set; }
        public string Descripcio { get; set; }
        public TipusCarta TipusDeCarta { get; set; }

        public CartaEnergiaEntrenador()
        {

        }

        public CartaEnergiaEntrenador(string nom, string desc, TipusCarta tc)
        {
            this.Nom = nom;
            this.Descripcio = desc;
            this.TipusDeCarta = tc;
        }
    }

    public enum TipusCarta
    {
        Energia,
        Entrenador
    }
}