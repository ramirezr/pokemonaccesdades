﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PokemonProjecte.Models
{
    [Table("JugadorTable")]
    public class Jugador
    {
        [Key]
        public int JugadorID { get; set; }

        [MaxLength(10)]
        public string Alias { get; set; }

        [Column("NumVictories")]
        public int Victories { get; set; }

        public Jugador()
        {
        }

        public Jugador(string alias)
        {
            Alias = alias;
            Victories = 0;
        }

        public override string ToString()
        {
            return "{ Jugador: { JugadorID: " + JugadorID + ", Alias: " + Alias + ", Victories: " + Victories + "}}";
        }
    }
}
