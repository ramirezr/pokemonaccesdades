﻿namespace PokemonProjecte.Models
{
    public class CartaPokemon
    {
        public int CartaPokemonID { get; set; }
        public string Nom { get; set; }
        public TipusEvolucio TipusEvolucio { get; set; }
        public int HP { get; set; }
        public int? PokemonEvolucioId { get; set; }
        public virtual CartaPokemon PokemonEvolucio { get; set; }

        public CartaPokemon()
        {
        }

        public CartaPokemon(string nom, int hp, TipusEvolucio te)
        {
            this.Nom = nom;
            this.HP = hp;
            this.TipusEvolucio = te;
        }
    }
}
public enum TipusEvolucio{
    Basic,
    Nivell1,
    Nivell2,
}