﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonProjecte.Models
{
    public class AccioPokemon
    {
        public int AccioPokemonID { get; set; }
        public int CartaPokemonID { get; set; }
        public virtual CartaPokemon CartaPokemon { get; set; }
        public int CartaEnergiaEntrenadorID { get; set; }
        public virtual CartaEnergiaEntrenador CartaEnergiaEntrenador { get; set; }
        public int NumCartesNecesariesEnergia { get; set; }
        public Accio Accio { get; set; }
        public int? ValorAccio { get; set; }

        public AccioPokemon()
        {

        }

        public AccioPokemon(CartaPokemon cartaPokemon, CartaEnergiaEntrenador cartaEnergiaEntrenador, int numCartesNecesariesEnergia, Accio accio, int? valorAccio)
        {
            CartaPokemon = cartaPokemon;
            CartaEnergiaEntrenador = cartaEnergiaEntrenador;
            NumCartesNecesariesEnergia = numCartesNecesariesEnergia;
            Accio = accio;
            ValorAccio = valorAccio;
        }
    }

    public enum Accio
    {
        Atac,
        Debilitat,
        Evolucio
    }
}
