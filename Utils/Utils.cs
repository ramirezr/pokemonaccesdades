﻿using PokemonProjecte.ContextUtils;
using PokemonProjecte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonProjecte
{
    internal static class Utils
    {
        #region metodos del main!
        //descarta cartas de la mano si tienes demasiadas en la mano
        public static bool descartarSiDemasiadasCartas(Context ctx, Random rnd, int i)
        {
            IList<Partida> cartasManoJugador = ctx.Partidas.Where(x => x.Estat == Estat.Ma && x.JugadorID == i).ToList();
            if (cartasManoJugador.Count() > 15)
            {
                return true;
            }
            return false;
        }

        

        //si no hay cartas en la pila, se cogen todas las de descartes y se ponen en la pila
        public static void resetPila(Context ctx, int i)
        {
            if (ctx.Partidas.Where(x => x.Estat == Estat.Pila && x.JugadorID == i).Count() == 0)
            {
                ctx.Partidas.Where(x => x.Estat == Estat.Descartada && x.JugadorID == i).ToList().ForEach(x => x.Estat = Estat.Pila);
                Console.WriteLine("S'ha fet reset de la pila del jugador " + ctx.Jugadors.Find(i).Alias);
                ctx.SaveChanges();
            }
        }
        //Comprobamos que el jugador tiene una carta pokemon basica en la mano almenos. true = si / false = no
        public static bool hasBasica(Context ctx, int i)
        {
            return ctx.Partidas.Where(x => x.JugadorID == i && x.Estat == Estat.Ma).Count(x => x.CartaPokemon.TipusEvolucio == TipusEvolucio.Basic) > 0;
        }

        //comprobamos que el jugador tiene una carta activa de tipo pokemon! true = si / false = no
        public static bool hasActiva(Context ctx, int i)
        {
            return ctx.Partidas.Where(x => x.Estat == Estat.Activa && x.JugadorID == i).Count(x => x.CartaPokemon != null) == 1;
        }
        //pone la primera carta que tengas de la mano basica en activa
        public static void setActiva(Context ctx, int i)
        {
            Partida p = ctx.Partidas.Where(x => x.Estat == Estat.Ma && x.JugadorID == i && x.CartaPokemon != null && x.CartaPokemon.TipusEvolucio == TipusEvolucio.Basic).FirstOrDefault();
            p.Estat = Estat.Activa;


            Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " activa la carta pokemon bàsica " + p.CartaPokemon.Nom + "\n");
            ctx.SaveChanges();
        }
        //pone carta de energia en la mesa si tiene en la mano y si coincide con la del pokemon.
        public static bool setEnergia(Context ctx, int i)
        {
            CartaPokemon cp = ctx.Partidas.Where(x => x.Estat == Estat.Activa && x.JugadorID == i).Select(x => x.CartaPokemon).FirstOrDefault();
            if (cp != null)
            {
                AccioPokemon ap = ctx.AccioPokemons.Where(x => x.CartaPokemon.CartaPokemonID == cp.CartaPokemonID && x.Accio == Accio.Atac).FirstOrDefault();
                CartaEnergiaEntrenador ce = ap.CartaEnergiaEntrenador;
                if (ce != null)
                {
                    Partida p = ctx.Partidas.Where(x => x.Estat == Estat.Ma && x.JugadorID == i && x.CartaEnergiaEntrenador.CartaEnergiaEntrenadorID == ce.CartaEnergiaEntrenadorID).FirstOrDefault();
                    if (p != null)
                    {
                        p.Estat = Estat.Activa;
                        Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " ha jugat la carta energia '" + ce.Nom + "'" + " sobre el pokemon " + cp.Nom);
                        ctx.SaveChanges();
                        return true;
                    }
                }
            }
            return false;
        }
        //a partir de un random del main, descartamos una carta (o no) de la mano
        public static void descartarCartaMano(Context ctx, Random rnd, int i)
        {
            IList<Partida> partidaList = ctx.Partidas.Where(x => (x.Estat == Estat.Ma || (x.CartaPokemon != null && x.Estat == Estat.Activa)) && x.JugadorID == i).ToList();
            int rand = rnd.Next(partidaList.Count());

            Partida p = partidaList[rand];
            if (p.Estat == Estat.Activa)
            {

                ctx.Partidas.Where(x => x.Estat == Estat.Activa && x.JugadorID == i).ToList().ForEach(x =>
                {
                    x.Estat = Estat.Descartada;
                    if (x.CartaPokemon != null)
                    {
                        p.Vida = p.CartaPokemon.HP;
                        Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " descarta una carta! (carta Pokemon: " + x.CartaPokemon.Nom + ")");
                    }
                    if (x.CartaEnergiaEntrenador != null)
                    {
                        if (x.CartaEnergiaEntrenador.TipusDeCarta == TipusCarta.Energia)
                            Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " descarta una carta! (carta energia: " + x.CartaEnergiaEntrenador.Nom + ")");
                        else
                            Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " descarta una carta! (carta entrenador: " + x.CartaEnergiaEntrenador.Nom + ")");
                    }


                });
            }
            else
            {
                if (p.CartaEnergiaEntrenador == null || p.CartaEnergiaEntrenador.TipusDeCarta == TipusCarta.Energia)
                {
                    p.Estat = Estat.Descartada;

                    if (p.CartaPokemon != null)
                        Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " descarta una que no està activa! (carta pokemon: " + p.CartaPokemon.Nom + ")");
                    else
                        Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " descarta una que no està activa! (carta energia: " + p.CartaEnergiaEntrenador.Nom + ")");
                }
                else
                {
                    int a = rnd.Next(2);
                    if (a == 1 && hasActiva(ctx, i))
                    {
                        CartaEnergiaEntrenador ce = p.CartaEnergiaEntrenador;
                        usarCartaEntrenador(ctx, i, ce);
                    }
                }
            }

            ctx.SaveChanges();
        }
        //intentar hacer evolucionar al pokemon activo (si tiene suficiente energia) (Random = 1 Evoluciona / Random = 0 No Evoluciona)
        public static bool canEvolve(Context ctx, Random rnd, int j)
        {

            IList<Partida> pokemonMano = ctx.Partidas.Where(x => x.Estat == Estat.Ma && x.JugadorID == j && x.CartaPokemon != null).ToList();

            bool state = false;


            if (pokemonMano.Count() > 0)
            {
                CartaPokemon pokemonActivo = ctx.Partidas
                .Where(x => x.Estat == Estat.Activa && x.CartaPokemon != null && x.JugadorID == j)
                .Select(x => x.CartaPokemon).FirstOrDefault();

                IList<AccioPokemon> accionsEvolucio = ctx.AccioPokemons.Where(x => x.Accio == Accio.Evolucio).ToList();

                AccioPokemon accio = null;

                foreach (AccioPokemon p in accionsEvolucio)
                {
                    if (p.CartaPokemonID == pokemonActivo.CartaPokemonID)
                    {
                        accio = p;
                    }
                }

                if (accio != null)
                {
                    CartaPokemon pokemonAlQueEvoluciona = ctx.CartaPokemons.Find(pokemonActivo.CartaPokemonID).PokemonEvolucio;
                    if (pokemonAlQueEvoluciona != null)
                    {
                        foreach (Partida p in pokemonMano)
                        {
                            if (p.CartaPokemonId == pokemonAlQueEvoluciona.CartaPokemonID)
                            {
                                Partida pokemonPartidaAlQueEvoluciona = p;
                                int cuantasEnergiasActivas = ctx.Partidas.Where(x => x.CartaEnergiaEntrenador != null && x.JugadorID == j).Count();
                                if (cuantasEnergiasActivas >= accio.NumCartesNecesariesEnergia)
                                {
                                    if (rnd.Next(2) == 1)
                                    {
                                        Partida pokemonPartidaQueEvoluciona = ctx.Partidas.Where(x => x.JugadorID == j && x.Estat == Estat.Activa && x.CartaPokemon != null).First();

                                        pokemonPartidaQueEvoluciona.Vida = pokemonPartidaQueEvoluciona.CartaPokemon.HP;
                                        pokemonPartidaQueEvoluciona.Estat = Estat.Descartada;

                                        pokemonPartidaAlQueEvoluciona.Estat = Estat.Activa;
                                        ctx.SaveChanges();
                                        Console.WriteLine("El pokemon de " + ctx.Jugadors.Find(j).Alias + " actiu, " + pokemonActivo.Nom + " evoluciona a " + pokemonAlQueEvoluciona.Nom + "!");
                                        state = true;
                                    }
                                    else
                                    {
                                        Console.WriteLine("El jugador " + ctx.Jugadors.Find(j).Alias + " pot evolucionar el seu pokemon actiu " + pokemonActivo.Nom + " a " + pokemonAlQueEvoluciona.Nom + " pero decideix no fer-lo!");
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("El " + pokemonActivo.Nom + " no pot evolucionar a res...");
                }
            }

            return state;
        }
        //usa la carta de entrenador para el pokemon activo acual
        public static void usarCartaEntrenador(Context ctx, int i, CartaEnergiaEntrenador c)
        {
            switch (c.Nom)
            {
                case "Augmentar_Punts":
                    ctx.Partidas.Where(x => x.Estat == Estat.Activa && x.JugadorID == i && x.CartaPokemon != null).FirstOrDefault().Vida += 500;
                    Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " juga la carta del tipus entrenador: " + c.Nom + ". Augmenta 500 punts de vida");
                    break;
                case "Robar_Pila":
                    Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " juga la carta del tipus entrenador: " + c.Nom);
                    resetPila(ctx, i);
                    robarCarta(ctx, new Random(), i);
                    break;
                case "Augmentar_força":
                    Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " juga la carta del tipus entrenador: " + c.Nom + ". Augmenta 100 puntos de força");
                    CartaPokemon cp = ctx.Partidas
                        .Where(x => x.Estat == Estat.Activa && x.JugadorID == i && x.CartaPokemon != null)
                        .Select(x => x.CartaPokemon).FirstOrDefault();
                    ctx.AccioPokemons.Where(x => x.CartaPokemon.CartaPokemonID == cp.CartaPokemonID && x.Accio == Accio.Atac).FirstOrDefault().ValorAccio += 100;
                    break;
                case "Intercanvi":
                    Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " juga la carta del tipus entrenador: " + c.Nom);
                    IList<Partida> manoJugador1 = ctx.Partidas
                        .Where(x => x.JugadorID == 1 && x.Estat == Estat.Ma).ToList();
                    IList<Partida> manoJugador2 = ctx.Partidas
                        .Where(x => x.JugadorID == 2 && x.Estat == Estat.Ma).ToList();

                    Partida p1 = manoJugador1[new Random().Next(manoJugador1.Count())];
                    Partida p2 = manoJugador2[new Random().Next(manoJugador2.Count())];

                    p1.JugadorID = 2;
                    p2.JugadorID = 1;
                    break;
                case "Comodi_Energia":
                    CartaPokemon cpp = ctx.Partidas.Where(x => x.Estat == Estat.Activa && x.JugadorID == i && x.CartaPokemon != null).Select(x => x.CartaPokemon).FirstOrDefault();

                    AccioPokemon ap = ctx.AccioPokemons.Where(x => x.CartaPokemon.CartaPokemonID == cpp.CartaPokemonID && x.Accio == Accio.Atac).FirstOrDefault();
                    CartaEnergiaEntrenador cc = ap.CartaEnergiaEntrenador;
                    Partida np = new Partida(ctx.Jugadors.Find(i), cc);
                    np.Estat = Estat.Activa;
                    Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " juga la carta del tipus entrenador: " + c.Nom);
                    ctx.Partidas.Add(np);
                    break;
                default:
                    break;
            }
            ctx.Partidas.Where(x => x.CartaEnergiaEntrenador.CartaEnergiaEntrenadorID == c.CartaEnergiaEntrenadorID).FirstOrDefault().Estat = Estat.Descartada;
            Console.WriteLine("Després de fer-la servir, s'ha descartat la carta " + c.Nom);
            ctx.SaveChanges();
        }
        //comprovem que es te la energia necesaria per atacar
        public static int hasEnergiaParaAtacar(Context ctx, int i)
        {
            IList<Partida> cartasEnergiaActivas = ctx.Partidas.Where(x => x.Estat == Estat.Activa && x.CartaEnergiaEntrenadorId != null).ToList();
            return cartasEnergiaActivas.Count();
        }
        //atac pokemon
        public static void atacPokemon(Context ctx, int i)
        {
            int idAtacant = i;
            int idDefensa = 1;
            if (idAtacant == 1)
            {
                idDefensa = 2;
            }
            CartaPokemon pokemonActiuAtacant = ctx.Partidas
                .Where(x => x.Estat == Estat.Activa && x.CartaPokemon != null && x.JugadorID == idAtacant)
                .Select(x => x.CartaPokemon)
                .DefaultIfEmpty()
                .First();

            CartaPokemon pokemonActiuDefendent = ctx.Partidas
                .Where(x => x.Estat == Estat.Activa && x.CartaPokemon != null && x.JugadorID == idDefensa)
                .Select(x => x.CartaPokemon)
                .DefaultIfEmpty()
                .First();

            AccioPokemon atacant = ctx.AccioPokemons
                .Where(x => x.CartaPokemon.CartaPokemonID == pokemonActiuAtacant.CartaPokemonID && x.Accio == Accio.Atac)
                .FirstOrDefault();
            AccioPokemon defendent = ctx.AccioPokemons
                .Where(x => x.CartaPokemon.CartaPokemonID == pokemonActiuDefendent.CartaPokemonID && x.Accio == Accio.Debilitat)
                .FirstOrDefault();

            int quantitatEnergiasNecesarias = atacant.NumCartesNecesariesEnergia;
            if (quantitatEnergiasNecesarias <= hasEnergiaParaAtacar(ctx, i))
            {
                Console.WriteLine(" \n<==> El pokemon " + pokemonActiuAtacant.Nom + " ataca a " + pokemonActiuDefendent.Nom + " <==>\n");
                int dany = atacant.ValorAccio.Value;
                //si es debil al tipo hace el doble de daño
                if (atacant.CartaEnergiaEntrenador == defendent.CartaEnergiaEntrenador)
                {
                    Console.WriteLine("ATENCIÓ! El pokemon defendent " + pokemonActiuDefendent.Nom + " es debil contra " + atacant.CartaEnergiaEntrenador.Nom + ", l'atac sera dur...");
                    dany = dany * 2;
                }

                Partida pokemonDefensor = ctx.Partidas
                    .Where(x => x.Estat == Estat.Activa && x.CartaPokemon != null && x.JugadorID == idDefensa)
                    .FirstOrDefault();
                pokemonDefensor.Vida -= dany;
                ctx.SaveChanges();
                if (pokemonDefensor.Vida > 0)
                {
                    Console.WriteLine("El pokemon " + pokemonActiuAtacant.Nom + " ha atacat al pokemon " + pokemonActiuDefendent.Nom + ". Li ha tret " + dany + " punts de vida.");
                }
                else
                {
                    Console.WriteLine("El pokemon de la defensa " + pokemonActiuDefendent.Nom + " s'ha debilitat del tot. " + ctx.Jugadors.Find(idDefensa).Alias + " ha perdut un pokemon.");

                    ctx.Partidas
                        .Where(x => x.Estat == Estat.Activa && x.JugadorID == idDefensa)
                        .ToList().ForEach(x => x.Estat = Estat.Descartada);
                    pokemonDefensor.Vida = pokemonActiuDefendent.HP;

                    Jugador jugadorAtacant = ctx.Jugadors.Find(idAtacant);
                    jugadorAtacant.Victories += 1;
                    ctx.SaveChanges();
                    if (hasBasica(ctx, idDefensa))
                    {
                        setActiva(ctx, idDefensa);
                    }
                }
            }
            else
            {
                Console.WriteLine("El pokemon que vol atacar (" + pokemonActiuAtacant.Nom + ") no té energies suficients per atacar.");
            }

            ctx.SaveChanges();


        }
        //cambia el turno del jugador cuando toque.
        public static int cambiarTurnJugador(Context ctx, int turnJugador)
        {
            return turnJugador == 1 ? 2 : 1;
        }
        #endregion
        //comença el joc, es distribueixen les cartes 7 als jugadors i es comprova que tingui un pokemon de nivell bàsic.
        public static void Inici(Context ctx)
        {
            #region Preparativos para poder empezar la partida.
            Random rnd = new Random();
            Console.WriteLine("Robant cartes de la pila...");
            //ambos jugadores roban cartas
            for (int i = 1; i <= 2; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    Utils.robarCarta(ctx, rnd, i);
                }
                Console.WriteLine("            --------             ");
            }
            //comprobamos que los jugadores tienen almenos una carta basica en la mano
            for (int i = 1; i <= 2; i++)
            {
                Console.WriteLine("Comprovant si el jugador " + ctx.Jugadors.Find(i).Alias + " té una carta bàsica...");
                while (!Utils.hasBasica(ctx, i))
                {
                    Console.WriteLine("     - " + ctx.Jugadors.Find(i).Alias + " no té una bàsica. Robarà 7 cartes.");
                    Console.WriteLine("     - L'altre jugador robarà 1 carta. ");

                    for (int j = 0; j < 7; j++)
                    {
                        Utils.resetPila(ctx, i);
                        Utils.robarCarta(ctx, rnd, i);
                    }
                    //turnJugador == 1 ? robarCarta(ctx, rnd, 2) : robarCarta(ctx, rnd, 1);
                    if (i == 1)
                    {
                        Utils.robarCarta(ctx, rnd, 2);
                    }
                    else if (i == 2)
                    {
                        Utils.robarCarta(ctx, rnd, 1);
                    }
                }
                Console.WriteLine("     - " + ctx.Jugadors.Find(i).Alias + " té una carta bàsica!");
                //cartas de la mano del jugador "i" que sean pokemon y basicas, coge la primera que veas i ponla a Activa
                Utils.setActiva(ctx, i);
                while (Utils.descartarSiDemasiadasCartas(ctx, rnd, i))
                {
                    Utils.descartarCartaMano(ctx, rnd, i);
                }
            }
            #endregion
        }
        //Robar una carta de la pila. Previament, comprova que hi ha cartes a la pila. En cas
        public static void robarCarta(Context ctx, Random rnd, int i)
        {
            Utils.resetPila(ctx, i);
            IList<Partida> partidaJugador = ctx.Partidas.Where(x => x.JugadorID == i && x.Estat == Estat.Pila).ToList();

            IList<Partida> cartasManoJugador = ctx.Partidas.Where(x => x.Estat == Estat.Ma && x.JugadorID == i).ToList();

            if (partidaJugador.Count() == 0)
            {
                Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " no tiene cartas para robar!, las tiene activas o en mano!");
            }
            else
            {
                int pj1 = rnd.Next(partidaJugador.Count());
                partidaJugador[pj1].Estat = Estat.Ma;

                if (partidaJugador[pj1].CartaPokemon != null)
                    Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " roba una carta: " + partidaJugador[pj1].CartaPokemon.Nom + ". És de tipus pokemon.");
                else
                    Console.WriteLine("El jugador " + ctx.Jugadors.Find(i).Alias + " roba una carta: " + partidaJugador[pj1].CartaEnergiaEntrenador.Nom + ". És de tipus " + partidaJugador[pj1].CartaEnergiaEntrenador.TipusDeCarta + ".");

                ctx.SaveChanges();
            }

        }
        //Resol les cartes de la mà del jugador, com descartar cartes, unir energies, evolucionar pokemon, etc.
        public static void PreviAtac(Context ctx, int turnJugador)
        {
            Random rnd = new Random();
            #region Si no tiene activa y acaba de robar una basica la activa
            if (!Utils.hasActiva(ctx, turnJugador) && Utils.hasBasica(ctx, turnJugador))
            {
                Utils.setActiva(ctx, turnJugador);
            }
            #endregion
            #region En caso de que ya tengamos activa
            else if (Utils.hasActiva(ctx, turnJugador))
            {
                #region Queremos descartar? 
                if (rnd.Next(2) == 1)
                {
                    Utils.descartarCartaMano(ctx, rnd, turnJugador);
                }
                #endregion
                #region si tengo carta activa en la mesa, intento asociarle una energia de mi mano si tengo
                if (!Utils.setEnergia(ctx, turnJugador) && Utils.hasActiva(ctx, turnJugador))
                {
                    Utils.canEvolve(ctx, rnd, turnJugador);
                }
                #endregion
            }
            #endregion
        }
        //Ataca i resol l’atac.
        public static void Atac(Context ctx, int turnJugador)
        {
            Random rnd = new Random();
            #region Atacar!
            if (Utils.hasActiva(ctx, 1) && Utils.hasActiva(ctx, 2))
            {
                int intAtacar = rnd.Next(2);
                if (intAtacar == 1 && Utils.hasEnergiaParaAtacar(ctx, turnJugador) > 0)
                {
                    Utils.atacPokemon(ctx, turnJugador);
                }
            }
            #endregion
        }
        //Fi de torn. Comprova si algun jugador té 6 victòries.
        public static bool FiTorn(Context ctx)
        {
            return ctx.Jugadors.Where(x => x.Victories < 6).Count() == 2;
        }
    }

    
}
