﻿using PokemonProjecte.ContextUtils;
using PokemonProjecte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonProjecte
{
    internal static class CargarDataBase
    {
        public static void insertDatos(Context ctx)
        {
            #region Carga Jugador
            Jugador amina = new Jugador("Raul");
            Jugador adrian = new Jugador("Adrian");
            ctx.Jugadors.Add(adrian);
            ctx.Jugadors.Add(amina);
            ctx.SaveChanges();
           
            #endregion

            #region Carga Pokemons
            CartaPokemon Bulbasaur = new CartaPokemon("Bulbasaur", 45, TipusEvolucio.Basic);
            CartaPokemon Ivysaur = new CartaPokemon("Ivysaur", 60, TipusEvolucio.Nivell1);
            CartaPokemon Venasaur = new CartaPokemon("Venasaur", 80, TipusEvolucio.Nivell2);
            Bulbasaur.PokemonEvolucio = Ivysaur;
            Ivysaur.PokemonEvolucio = Venasaur;
            CartaPokemon Charmander = new CartaPokemon("Charmander", 39, TipusEvolucio.Basic);
            CartaPokemon Charmeleon = new CartaPokemon("Charmeleon", 58, TipusEvolucio.Nivell1);
            CartaPokemon Charizard = new CartaPokemon("Charizard", 78, TipusEvolucio.Nivell2);
            Charmander.PokemonEvolucio = Charmeleon;
            Charmeleon.PokemonEvolucio = Charizard;
            CartaPokemon Squirtle = new CartaPokemon("Squirtle", 44, TipusEvolucio.Basic);
            CartaPokemon Wartortle = new CartaPokemon("Wartortle", 59, TipusEvolucio.Nivell1);
            CartaPokemon Blastoise = new CartaPokemon("Blastoise", 79, TipusEvolucio.Nivell2);
            Squirtle.PokemonEvolucio = Wartortle;
            Wartortle.PokemonEvolucio = Blastoise;
            CartaPokemon Caterpie = new CartaPokemon("Caterpie", 45, TipusEvolucio.Basic);
            CartaPokemon Metapod = new CartaPokemon("Metapod", 50, TipusEvolucio.Nivell1);
            CartaPokemon Butterfree = new CartaPokemon("Butterfree", 60, TipusEvolucio.Nivell2);
            Caterpie.PokemonEvolucio = Metapod;
            Metapod.PokemonEvolucio = Butterfree;
            CartaPokemon Weedle = new CartaPokemon("Weedle", 40, TipusEvolucio.Basic);
            CartaPokemon Kakuna = new CartaPokemon("Kakuna", 45, TipusEvolucio.Nivell1);
            CartaPokemon Beedrill = new CartaPokemon("Beedrill", 65, TipusEvolucio.Nivell2);
            Weedle.PokemonEvolucio = Kakuna;
            Kakuna.PokemonEvolucio = Beedrill;
            CartaPokemon Ekans = new CartaPokemon("Ekans", 35, TipusEvolucio.Basic);
            CartaPokemon Arbok = new CartaPokemon("Arbok", 60, TipusEvolucio.Nivell1);
            Ekans.PokemonEvolucio = Arbok;
            CartaPokemon Pichu = new CartaPokemon("Pichu", 20, TipusEvolucio.Basic);
            CartaPokemon Pikachu = new CartaPokemon("Pikachu", 45, TipusEvolucio.Nivell1);
            CartaPokemon Raichu = new CartaPokemon("Raichu", 60, TipusEvolucio.Nivell2);
            Pichu.PokemonEvolucio = Pikachu;
            Pikachu.PokemonEvolucio = Raichu;
            CartaPokemon Sandshrew = new CartaPokemon("Sandshrew", 50, TipusEvolucio.Basic);
            CartaPokemon Sandslash = new CartaPokemon("Sandslash", 75, TipusEvolucio.Nivell1);
            Sandshrew.PokemonEvolucio = Sandslash;
            CartaPokemon NidoranHembra = new CartaPokemon("Nidoran", 55, TipusEvolucio.Basic);
            CartaPokemon Nidorina = new CartaPokemon("Nidorina", 70, TipusEvolucio.Nivell1);
            CartaPokemon Nidoqueen = new CartaPokemon("Nidoqueen", 90, TipusEvolucio.Nivell2);
            NidoranHembra.PokemonEvolucio = Nidorina;
            Nidorina.PokemonEvolucio = Nidoqueen;
            CartaPokemon NidoranMacho = new CartaPokemon("Nidoran", 46, TipusEvolucio.Basic);
            CartaPokemon Nidorino = new CartaPokemon("Nidorino", 61, TipusEvolucio.Nivell1);
            CartaPokemon Nidoking = new CartaPokemon("Nidoking", 81, TipusEvolucio.Nivell2);
            NidoranMacho.PokemonEvolucio = Nidorino;
            Nidorino.PokemonEvolucio = Nidoking;
            CartaPokemon Clefairy = new CartaPokemon("Clefairy", 70, TipusEvolucio.Basic);
            CartaPokemon Clefable = new CartaPokemon("Clefable", 90, TipusEvolucio.Nivell1);
            Clefairy.PokemonEvolucio = Clefable;
            CartaPokemon Vulpix = new CartaPokemon("Vulpix", 38, TipusEvolucio.Basic);
            CartaPokemon Ninetales = new CartaPokemon("Ninetales", 73, TipusEvolucio.Nivell1);
            Vulpix.PokemonEvolucio = Ninetales;
            CartaPokemon Iglybuff = new CartaPokemon("Iglybuff", 90, TipusEvolucio.Basic);
            CartaPokemon Jigglypuff = new CartaPokemon("Jigglypuff", 115, TipusEvolucio.Nivell1);
            CartaPokemon Wigglytuff = new CartaPokemon("Wigglytuff", 140, TipusEvolucio.Nivell2);
            Iglybuff.PokemonEvolucio = Jigglypuff;
            Jigglypuff.PokemonEvolucio = Wigglytuff;
            CartaPokemon Zubat = new CartaPokemon("Zubat", 40, TipusEvolucio.Basic);
            CartaPokemon Golbat = new CartaPokemon("Golbat", 75, TipusEvolucio.Nivell1);
            CartaPokemon Crobat = new CartaPokemon("Crobat", 85, TipusEvolucio.Nivell2);
            Zubat.PokemonEvolucio = Golbat;
            Golbat.PokemonEvolucio = Crobat;
            CartaPokemon Paras = new CartaPokemon("Paras", 35, TipusEvolucio.Basic);
            CartaPokemon Parasect = new CartaPokemon("Parasect", 60, TipusEvolucio.Nivell1);
            Paras.PokemonEvolucio = Parasect;
            CartaPokemon Aron = new CartaPokemon("Aron", 40, TipusEvolucio.Basic);
            CartaPokemon Lairon = new CartaPokemon("Lairon", 75, TipusEvolucio.Nivell1);
            CartaPokemon Aggron = new CartaPokemon("Aggron", 120, TipusEvolucio.Nivell2);
            Aron.PokemonEvolucio = Lairon;
            Lairon.PokemonEvolucio = Aggron;
            CartaPokemon Dratini = new CartaPokemon("Dratini", 40, TipusEvolucio.Nivell1);
            CartaPokemon Dragonair = new CartaPokemon("Dragonair", 75, TipusEvolucio.Nivell1);
            CartaPokemon Dragonite = new CartaPokemon("Dragonite", 120, TipusEvolucio.Nivell1);
            Dratini.PokemonEvolucio = Dragonair;
            Dragonair.PokemonEvolucio = Dragonite;
            CartaPokemon Gastly = new CartaPokemon("Gastly", 40, TipusEvolucio.Nivell1);
            CartaPokemon Haunter = new CartaPokemon("Haunter", 75, TipusEvolucio.Nivell1);
            CartaPokemon Gengar = new CartaPokemon("Gengar", 120, TipusEvolucio.Nivell1);
            Gastly.PokemonEvolucio = Haunter;
            Haunter.PokemonEvolucio = Gengar;


            //carga bbdd
            ctx.CartaPokemons.Add(Bulbasaur);
            ctx.CartaPokemons.Add(Ivysaur);
            ctx.CartaPokemons.Add(Venasaur);
            ctx.CartaPokemons.Add(Charmander);
            ctx.CartaPokemons.Add(Charmeleon);
            ctx.CartaPokemons.Add(Charizard);
            ctx.CartaPokemons.Add(Squirtle);
            ctx.CartaPokemons.Add(Wartortle);
            ctx.CartaPokemons.Add(Blastoise);
            ctx.CartaPokemons.Add(Caterpie);
            ctx.CartaPokemons.Add(Metapod);
            ctx.CartaPokemons.Add(Butterfree);
            ctx.CartaPokemons.Add(Weedle);
            ctx.CartaPokemons.Add(Kakuna);
            ctx.CartaPokemons.Add(Beedrill);
            ctx.CartaPokemons.Add(Ekans);
            ctx.CartaPokemons.Add(Arbok);
            ctx.CartaPokemons.Add(Pichu);
            ctx.CartaPokemons.Add(Pikachu);
            ctx.CartaPokemons.Add(Raichu);
            ctx.CartaPokemons.Add(Sandshrew);
            ctx.CartaPokemons.Add(Sandslash);
            ctx.CartaPokemons.Add(NidoranHembra);
            ctx.CartaPokemons.Add(Nidorina);
            ctx.CartaPokemons.Add(Nidoqueen);
            ctx.CartaPokemons.Add(NidoranMacho);
            ctx.CartaPokemons.Add(Nidorino);
            ctx.CartaPokemons.Add(Nidoking);
            ctx.CartaPokemons.Add(Clefairy);
            ctx.CartaPokemons.Add(Clefable);
            ctx.CartaPokemons.Add(Vulpix);
            ctx.CartaPokemons.Add(Ninetales);
            ctx.CartaPokemons.Add(Iglybuff);
            ctx.CartaPokemons.Add(Jigglypuff);
            ctx.CartaPokemons.Add(Wigglytuff);
            ctx.CartaPokemons.Add(Zubat);
            ctx.CartaPokemons.Add(Golbat);
            ctx.CartaPokemons.Add(Crobat);
            ctx.CartaPokemons.Add(Paras);
            ctx.CartaPokemons.Add(Parasect);
            ctx.CartaPokemons.Add(Aron);
            ctx.CartaPokemons.Add(Lairon);
            ctx.CartaPokemons.Add(Aggron);
            ctx.CartaPokemons.Add(Dratini);
            ctx.CartaPokemons.Add(Dragonair);
            ctx.CartaPokemons.Add(Dragonite);
            ctx.CartaPokemons.Add(Gastly);
            ctx.CartaPokemons.Add(Haunter);
            ctx.CartaPokemons.Add(Gengar);

            ctx.SaveChanges();
	        #endregion

            #region Carga Carta Energia
            CartaEnergiaEntrenador Aigua = new CartaEnergiaEntrenador("Aigua", "Realitza atacs de tipus d'aigua.", TipusCarta.Energia);
            CartaEnergiaEntrenador Planta = new CartaEnergiaEntrenador("Planta", "Utilitzada pels pokemon de tipus planta i animal.", TipusCarta.Energia);
            CartaEnergiaEntrenador Foc = new CartaEnergiaEntrenador("Foc", "Realitza atacs de tipus foc.", TipusCarta.Energia);
            CartaEnergiaEntrenador Llamp = new CartaEnergiaEntrenador("Llamp", "Utilitza els pokemons elèctrics", TipusCarta.Energia);
            CartaEnergiaEntrenador Lluita = new CartaEnergiaEntrenador("Lluita", "Representa els tipus de lluita, terra i roca.", TipusCarta.Energia);
            CartaEnergiaEntrenador Psiquica = new CartaEnergiaEntrenador("Psiquica", "Utilitzada pels pokemon de tipus psiquic, fantasma i verí.", TipusCarta.Energia);
            CartaEnergiaEntrenador Metalica = new CartaEnergiaEntrenador("Metalica", "Les utilitza els pokemons de tipus d'acer", TipusCarta.Energia);
            CartaEnergiaEntrenador Fosca = new CartaEnergiaEntrenador("Fosca", "Augmenta el dany ocasionat per un altre pokemon", TipusCarta.Energia);
            CartaEnergiaEntrenador Drac = new CartaEnergiaEntrenador("Drac", "No es troba en atacs, però si en debilitats", TipusCarta.Energia);
            CartaEnergiaEntrenador Fada = new CartaEnergiaEntrenador("Fada", "Realitza atacs d'energia de tipus fada", TipusCarta.Energia);
            //cargabbdd
            ctx.CartaEnergiaEntrenadors.Add(Aigua);
            ctx.CartaEnergiaEntrenadors.Add(Planta);
            ctx.CartaEnergiaEntrenadors.Add(Foc);
            ctx.CartaEnergiaEntrenadors.Add(Llamp);
            ctx.CartaEnergiaEntrenadors.Add(Lluita);
            ctx.CartaEnergiaEntrenadors.Add(Psiquica);
            ctx.CartaEnergiaEntrenadors.Add(Metalica);
            ctx.CartaEnergiaEntrenadors.Add(Fosca);
            ctx.CartaEnergiaEntrenadors.Add(Drac);
            ctx.CartaEnergiaEntrenadors.Add(Fada);
            ctx.SaveChanges();
            #endregion

            #region Carga Carta Entrenador
            CartaEnergiaEntrenador AugPunts = new CartaEnergiaEntrenador("Augmentar_Punts", "Agumenta 500 punts els punts HP del pokemon.", TipusCarta.Entrenador);
            CartaEnergiaEntrenador RobarPila = new CartaEnergiaEntrenador("Robar_Pila", "Roba una carta adicional de la pila.", TipusCarta.Entrenador);
            CartaEnergiaEntrenador AugFuerza = new CartaEnergiaEntrenador("Augmentar_força", "Agumenta la força amb 100 punts l'atac.", TipusCarta.Entrenador);
            CartaEnergiaEntrenador Intercanvi = new CartaEnergiaEntrenador("Intercanvi", "Intercanvia una carta amb l'adversari.", TipusCarta.Entrenador);
            CartaEnergiaEntrenador ComEnergia = new CartaEnergiaEntrenador("Comodi_Energia", "Carta comodi energia.", TipusCarta.Entrenador);
            //cargabbdd
            ctx.CartaEnergiaEntrenadors.Add(AugPunts);
            ctx.CartaEnergiaEntrenadors.Add(RobarPila);
            ctx.CartaEnergiaEntrenadors.Add(AugFuerza);
            ctx.CartaEnergiaEntrenadors.Add(Intercanvi);
            ctx.CartaEnergiaEntrenadors.Add(ComEnergia);
            ctx.SaveChanges();
            #endregion

            #region Accions Pokemon Atacar
            //load accionesPokemon
            IList<AccioPokemon> listAtac = new List<AccioPokemon> { 
                new AccioPokemon(Bulbasaur, Planta, 1, Accio.Atac, 50),
                new AccioPokemon(Ivysaur, Planta, 1, Accio.Atac, 100),
                new AccioPokemon(Venasaur, Planta, 1, Accio.Atac, 150),
                new AccioPokemon(Charmander, Foc, 1, Accio.Atac, 50),
                new AccioPokemon(Charmeleon, Foc, 1, Accio.Atac, 100),
                new AccioPokemon(Charizard, Foc, 1, Accio.Atac, 150),
                new AccioPokemon(Squirtle, Aigua, 1, Accio.Atac, 50),
                new AccioPokemon(Wartortle, Aigua, 1, Accio.Atac, 100),
                new AccioPokemon(Blastoise, Aigua, 1, Accio.Atac, 150),
                new AccioPokemon(Caterpie, Planta, 1, Accio.Atac, 50),
                new AccioPokemon(Metapod, Planta, 1, Accio.Atac, 75),
                new AccioPokemon(Butterfree, Planta, 1, Accio.Atac, 125),
                new AccioPokemon(Weedle, Planta, 1, Accio.Atac, 50),
                new AccioPokemon(Kakuna, Planta, 1, Accio.Atac, 75),
                new AccioPokemon(Beedrill, Planta, 1, Accio.Atac, 125),
                new AccioPokemon(Ekans, Psiquica, 1, Accio.Atac, 50),
                new AccioPokemon(Arbok, Psiquica, 1, Accio.Atac, 120),
                new AccioPokemon(Pichu, Llamp, 1, Accio.Atac, 50),
                new AccioPokemon(Pikachu, Llamp, 1, Accio.Atac, 80),
                new AccioPokemon(Raichu, Llamp, 1, Accio.Atac, 125),
                new AccioPokemon(Sandshrew, Lluita, 1, Accio.Atac, 50),
                new AccioPokemon(Sandslash, Lluita, 1, Accio.Atac, 100),
                new AccioPokemon(NidoranHembra, Lluita, 1, Accio.Atac, 50),
                new AccioPokemon(Nidorina, Lluita, 1, Accio.Atac, 75),
                new AccioPokemon(Nidoqueen, Lluita, 1, Accio.Atac, 100),
                new AccioPokemon(NidoranMacho, Lluita, 1, Accio.Atac, 50),
                new AccioPokemon(Nidorino, Lluita, 1, Accio.Atac, 75),
                new AccioPokemon(Nidoking, Lluita, 1, Accio.Atac, 100),
                new AccioPokemon(Clefairy, Fada, 1, Accio.Atac, 50),
                new AccioPokemon(Clefable, Fada, 1, Accio.Atac, 80),
                new AccioPokemon(Vulpix, Foc, 1, Accio.Atac, 50),
                new AccioPokemon(Ninetales, Foc, 1, Accio.Atac, 125),
                new AccioPokemon(Iglybuff, Fada, 1, Accio.Atac, 50),
                new AccioPokemon(Jigglypuff, Fada, 1, Accio.Atac, 70),
                new AccioPokemon(Wigglytuff, Fada, 1, Accio.Atac, 90),
                new AccioPokemon(Zubat, Fosca, 1, Accio.Atac, 50),
                new AccioPokemon(Golbat, Fosca, 1, Accio.Atac, 75),
                new AccioPokemon(Crobat, Fosca, 1, Accio.Atac, 95),
                new AccioPokemon(Paras, Planta, 1, Accio.Atac, 50),
                new AccioPokemon(Parasect, Planta, 1, Accio.Atac, 80),
                new AccioPokemon(Aron, Metalica, 1, Accio.Atac, 80),
                new AccioPokemon(Lairon, Metalica, 1, Accio.Atac, 80),
                new AccioPokemon(Aggron, Metalica, 1, Accio.Atac, 80),
                new AccioPokemon(Dratini, Drac, 1, Accio.Atac, 80),
                new AccioPokemon(Dragonair, Drac, 1, Accio.Atac, 80),
                new AccioPokemon(Dragonite, Drac, 1, Accio.Atac, 80),
                new AccioPokemon(Gastly, Fosca, 1, Accio.Atac, 80),
                new AccioPokemon(Haunter, Fosca, 1, Accio.Atac, 80),
                new AccioPokemon(Gengar, Fosca, 1, Accio.Atac, 80)
        };
            listAtac.ToList().ForEach(a => ctx.AccioPokemons.Add(a));
            #endregion

            #region Accions Pokemon Debilitat
            //load accionesPokemon
            IList<AccioPokemon> listDebilitat = new List<AccioPokemon> {
                new AccioPokemon(Bulbasaur, Foc, 1, Accio.Debilitat, 2),
                new AccioPokemon(Ivysaur, Foc, 1, Accio.Debilitat, 2),
                new AccioPokemon(Venasaur, Foc, 1, Accio.Debilitat, 2),
                new AccioPokemon(Charmander, Aigua, 1, Accio.Debilitat, 2),
                new AccioPokemon(Charmeleon, Aigua, 1, Accio.Debilitat, 2),
                new AccioPokemon(Charizard, Aigua, 1, Accio.Debilitat, 2),
                new AccioPokemon(Squirtle, Llamp, 1, Accio.Debilitat, 2),
                new AccioPokemon(Wartortle, Llamp, 1, Accio.Debilitat, 2),
                new AccioPokemon(Blastoise, Llamp, 1, Accio.Debilitat, 2),
                new AccioPokemon(Caterpie, Foc, 1, Accio.Debilitat, 2),
                new AccioPokemon(Metapod, Foc, 1, Accio.Debilitat, 2),
                new AccioPokemon(Butterfree, Foc, 1, Accio.Debilitat, 2),
                new AccioPokemon(Weedle, Foc, 1, Accio.Debilitat, 2),
                new AccioPokemon(Kakuna, Foc, 1, Accio.Debilitat, 2),
                new AccioPokemon(Beedrill, Foc, 1, Accio.Debilitat, 2),
                new AccioPokemon(Ekans, Lluita, 1, Accio.Debilitat, 2),
                new AccioPokemon(Arbok, Lluita, 1, Accio.Debilitat, 2),
                new AccioPokemon(Pichu, Psiquica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Pikachu, Psiquica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Raichu, Psiquica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Sandshrew, Planta, 1, Accio.Debilitat, 2),
                new AccioPokemon(Sandslash, Planta, 1, Accio.Debilitat, 2),
                new AccioPokemon(NidoranHembra, Metalica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Nidorina, Metalica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Nidoqueen, Metalica, 1, Accio.Debilitat, 2),
                new AccioPokemon(NidoranMacho, Metalica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Nidorino, Metalica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Nidoking, Metalica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Clefairy, Fada, 1, Accio.Debilitat, 2),
                new AccioPokemon(Clefable, Fada, 1, Accio.Debilitat, 2),
                new AccioPokemon(Vulpix, Aigua, 1, Accio.Debilitat, 2),
                new AccioPokemon(Ninetales, Aigua, 1, Accio.Debilitat, 2),
                new AccioPokemon(Iglybuff, Fada, 1, Accio.Debilitat, 2),
                new AccioPokemon(Jigglypuff, Fada, 1, Accio.Debilitat, 2),
                new AccioPokemon(Wigglytuff, Fada, 1, Accio.Debilitat, 2),
                new AccioPokemon(Zubat, Metalica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Golbat, Metalica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Crobat, Metalica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Paras, Foc, 1, Accio.Debilitat, 2),
                new AccioPokemon(Parasect, Foc, 1, Accio.Debilitat, 2),
                new AccioPokemon(Aron, Fosca , 1, Accio.Debilitat, 2),
                new AccioPokemon(Lairon, Fosca, 1, Accio.Debilitat, 2),
                new AccioPokemon(Aggron, Fosca, 1, Accio.Debilitat, 2),
                new AccioPokemon(Dratini, Drac, 1, Accio.Debilitat, 2),
                new AccioPokemon(Dragonair, Drac, 1, Accio.Debilitat, 2),
                new AccioPokemon(Dragonite, Drac, 1, Accio.Debilitat, 2),
                new AccioPokemon(Gastly, Psiquica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Haunter, Psiquica, 1, Accio.Debilitat, 2),
                new AccioPokemon(Gengar, Psiquica, 1, Accio.Debilitat, 2)
            };
            listDebilitat.ToList().ForEach(a => ctx.AccioPokemons.Add(a));
            #endregion

            #region Accions Pokemon Evolucio
            //load accionesPokemon
            IList<AccioPokemon> listEvolucio = new List<AccioPokemon> {
                new AccioPokemon(Bulbasaur, Planta, 1, Accio.Evolucio, null),
                new AccioPokemon(Ivysaur, Planta, 1, Accio.Evolucio, null),
                new AccioPokemon(Charmander, Foc, 1, Accio.Evolucio, null),
                new AccioPokemon(Charmeleon, Foc, 1, Accio.Evolucio, null),
                new AccioPokemon(Squirtle, Aigua, 1, Accio.Evolucio, null),
                new AccioPokemon(Wartortle, Aigua, 1, Accio.Evolucio, null),
                new AccioPokemon(Caterpie, Planta, 1, Accio.Evolucio, null),
                new AccioPokemon(Metapod, Planta, 1, Accio.Evolucio, null),
                new AccioPokemon(Weedle, Planta, 1, Accio.Evolucio, null),
                new AccioPokemon(Kakuna, Planta, 1, Accio.Evolucio, null),
                new AccioPokemon(Ekans, Psiquica, 1, Accio.Evolucio, null),
                new AccioPokemon(Pichu, Llamp, 1, Accio.Evolucio, null),
                new AccioPokemon(Pikachu, Llamp, 1, Accio.Evolucio, null),
                new AccioPokemon(Sandshrew, Lluita, 1, Accio.Evolucio, null),
                new AccioPokemon(NidoranHembra, Lluita, 1, Accio.Evolucio, null),
                new AccioPokemon(Nidorina, Lluita, 1, Accio.Evolucio, null),
                new AccioPokemon(NidoranMacho, Lluita, 1, Accio.Evolucio, null),
                new AccioPokemon(Nidorino, Lluita, 1, Accio.Evolucio, null),
                new AccioPokemon(Clefairy, Fada, 1, Accio.Evolucio, null),
                new AccioPokemon(Vulpix, Foc, 1, Accio.Evolucio, null),
                new AccioPokemon(Iglybuff, Fada, 1, Accio.Evolucio, null),
                new AccioPokemon(Jigglypuff, Fada, 1, Accio.Evolucio, null),
                new AccioPokemon(Zubat, Fosca, 1, Accio.Evolucio, null),
                new AccioPokemon(Golbat, Fosca, 1, Accio.Evolucio, null),
                new AccioPokemon(Paras, Planta, 1, Accio.Evolucio, null),
                new AccioPokemon(Aron, Metalica , 1, Accio.Evolucio, null),
                new AccioPokemon(Lairon, Metalica, 1, Accio.Evolucio, null),
                new AccioPokemon(Dratini, Drac, 1, Accio.Evolucio, null),
                new AccioPokemon(Dragonair, Drac, 1, Accio.Evolucio, null),
                new AccioPokemon(Gastly, Fosca, 1, Accio.Evolucio, null),
                new AccioPokemon(Haunter, Fosca, 1, Accio.Evolucio, null)
            };
            listEvolucio.ToList().ForEach(a => ctx.AccioPokemons.Add(a));
            #endregion

            #region Repartir cartas
            repartirCartas(ctx, 1);
            repartirCartas(ctx, 2);
            #endregion

            ctx.SaveChanges();
        }

        private static void repartirCartas(Context ctx, int idJugador)
        {
            IDictionary<int, int> cartasPokemonJugador1 = new Dictionary<int, int>();
            int i = 0;
            int pos = 0;
            Random rng = new Random();
            while (i < 20)
            {
                pos = rng.Next(1, ctx.CartaPokemons.Count() + 1);
                CartaPokemon carta = ctx.CartaPokemons.Find(pos);
                int value;
                bool hasValue = cartasPokemonJugador1.TryGetValue(carta.CartaPokemonID, out value);
                if (hasValue)
                {
                    if (value <= 4)
                    {
                        cartasPokemonJugador1[carta.CartaPokemonID]+=1;
                        Partida partida = new Partida(ctx.Jugadors.Find(idJugador), carta, carta.HP);
                        ctx.Partidas.Add(partida);
                        i++;
                    }
                }
                else
                {
                    cartasPokemonJugador1.Add(carta.CartaPokemonID, 1);
                    Partida partida = new Partida(ctx.Jugadors.Find(idJugador), carta, carta.HP);
                    ctx.Partidas.Add(partida);
                    i++;
                }
            }

            int times = 0;
            IList<CartaEnergiaEntrenador> ce = ctx.CartaEnergiaEntrenadors.Where(c => c.TipusDeCarta == TipusCarta.Energia).ToList();
            while (times < 15)
            {
                pos = rng.Next(ce.Count());
                CartaEnergiaEntrenador carta1 = ce[pos];
                ctx.Partidas.Add(new Partida(ctx.Jugadors.Find(idJugador), carta1));
                times++;
            }

            times = 0;
            ce = ctx.CartaEnergiaEntrenadors.Where(c => c.TipusDeCarta == TipusCarta.Entrenador).ToList();

            while (times < 5)
            {
                pos = rng.Next(ce.Count());
                CartaEnergiaEntrenador carta1 = ce[pos];
                ctx.Partidas.Add(new Partida(ctx.Jugadors.Find(idJugador), carta1));
                times++;
            }

            ctx.SaveChanges();
        }
    } 
}
