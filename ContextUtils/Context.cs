﻿using PokemonProjecte.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonProjecte.ContextUtils
{
    public class Context : DbContext
    {
        public Context() : base("pokemon")
        {

        }

        public DbSet<AccioPokemon> AccioPokemons { get; set; }
        public DbSet<CartaEnergiaEntrenador> CartaEnergiaEntrenadors { get; set; }

        public DbSet<CartaPokemon> CartaPokemons { get; set; }
        public DbSet<Jugador> Jugadors { get; set; }
        public DbSet<Partida> Partidas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<Context>());
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CartaPokemon>()
                .HasOptional(c => c.PokemonEvolucio)
                .WithMany()
                .HasForeignKey(c => c.PokemonEvolucioId);

        }
    }
}
