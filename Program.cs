﻿using PokemonProjecte.ContextUtils;
using PokemonProjecte.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonProjecte
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new Context())
            {

                Console.WriteLine("***********************************");
                Console.WriteLine("--- POKEMON CARTAS SIMULATOR ---");
                Console.WriteLine("***********************************");
                int turnJugador = 1;
                Random rnd = new Random();
                //cargamos la base de dades
                CargarDataBase.insertDatos(ctx);
                Console.WriteLine("Ja s'han carregat les dades! Cada jugador té la seva pila");
                

                int basicJugador1 = ctx.Partidas.Where(x => x.JugadorID == 1).Count(x => x.CartaPokemon.TipusEvolucio == TipusEvolucio.Basic);
                int basicJugador2 = ctx.Partidas.Where(x => x.JugadorID == 2).Count(x => x.CartaPokemon.TipusEvolucio == TipusEvolucio.Basic);
                //si uno de los jugadores no tiene cartas de tipo basico, la partida acaba inmediatamente
                //si tiene carta de tipo basica...
                if (basicJugador1 > 0 && basicJugador2 > 0)
                {
                    #region Inici()
                    Utils.Inici(ctx);
                    #endregion
                    #region Se juega la partida
                    //FiTorn
                    while (Utils.FiTorn(ctx))
                    {
                        #region Robar(), robar comprueba tambien si no hay cartas en la pila, y coge todo lo de descartes y lo pone en la pila
                        Utils.robarCarta(ctx, rnd, turnJugador);
                        #endregion
                        #region PreviAtac()
                        Utils.PreviAtac(ctx, turnJugador);
                        #endregion
                        #region Atac()
                        Utils.Atac(ctx, turnJugador);
                        #endregion
                        #region acaba el torn
                        Console.WriteLine("S'acabat el torn de " + ctx.Jugadors.Find(turnJugador).Alias + ". Ara li toca a l'altre jugador!\n");
                        turnJugador = Utils.cambiarTurnJugador(ctx, turnJugador);
                        #endregion
                    }
                    #endregion

                    #region Si uno de los jugadores alcanza las 6 victorias la partida acaba
                    if (!Utils.FiTorn(ctx))
                    {
                        Console.WriteLine("--- EP! Ha finalitzat la partida. EL GUANYADOR ÉS: --- ");
                        Jugador guanyador = ctx.Jugadors.Where(x => x.Victories == 6).FirstOrDefault();
                        Console.WriteLine("              " + guanyador.Alias.ToUpper() + "              ");
                        Console.WriteLine("             --- FELICITATS!!  ---         "    );
                        Console.ReadKey();
                    }
                    #endregion
                }
                #region Si uno de los jugadore sno tiene carta basica
                else
                {
                    if (basicJugador1 == 0)
                    {
                        Console.WriteLine("El Jugador " + ctx.Jugadors.Find(1).Alias + " no té cap carta bàsica a la pila. Ha perdut :(");
                    }
                    else
                    {
                        Console.WriteLine("El Jugador " + ctx.Jugadors.Find(2).Alias + " no té cap carta bàsica a la pila. Ha perdut :(");
                    }
                }
                #endregion
            }
        }
    }
}
